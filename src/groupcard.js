import { drawLight } from "./lightcard.js";

export const draw = (term, group) => {
    return new Promise(async (res, rej) => {
        term.bold("\n" + group.name + "\n");

        let lights = await group.getLights();

        //console.log(lights);
        
        let menuItems = [...lights.map(light => {
            return light.name;
        }),
        "All"
    ];

        let resp = await term.singleColumnMenu(menuItems, {
            cancelable: true
        }).promise;
        //console.log(lights[resp.selectedIndex]);
        
        
        if (resp.canceled) {
            rej();
        } else {
            try {

                if (resp.selectedIndex == lights.length) {
                    await drawLight(term, group, "action")
                } else {
                    //term.bold(lights[resp.selectedIndex] + "\n");
                    await drawLight(term, lights[resp.selectedIndex]);
                }
            } catch (e) {
                res(resp)
            }
        }

    });
}