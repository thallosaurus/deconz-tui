import tkit from 'terminal-kit';
import { getGroups } from 'deconz-typescript/dist/model/Group.js';
import { draw } from './groupcard.js';

const startScreen = (async () => {
    let term = tkit.terminal;
    term.grabInput();

    term.on("key", (key, matches, data) => {
        if (key == "CTRL_C") {
            process.exit(0);
        }
    });

    //Grad list of groups
    let groups = await getGroups();
    let menuItems = groups.map((group => {
        return group.name;
    }))

    let resp = await term.singleColumnMenu(menuItems).promise;
    let selectedItem = groups[resp.selectedIndex];

    try {

        await draw(term, selectedItem);
    } catch (e) {
    }
    startScreen();

});
startScreen();