import { colorpicker } from "./colorpicker.js";
import ColorConverter from 'ts-cie1931-rgb'; 
import { Group } from "deconz-typescript/dist/model/Group.js";

export const drawLight = (term, light, stateEndpoint = "state") => {
    return new Promise(async (back, rej) => {
        term.bold("\n" + light.name + "\n");

        let currentState = light[stateEndpoint].on;

        let menuItemsRaw = [
            {
                "name": `Light State: ${currentState ? "Off" : "On"}`,
                "callback": () => {
                    return new Promise((res, rej) => {

                        if (light[stateEndpoint].on) {
                            light.turnOff();
                        } else {
                            light.turnOn();
                        }
                        res();
                    })
                }
            }
        ];

        if (light.hascolor || light instanceof Group) {
            menuItemsRaw = [
                ...menuItemsRaw,
                {
                    "name": `Set Color`,
                    "callback": async () => {
                        return new Promise(async (res, rej) => {
                            try {
                                let [x, y] = await colorpicker(term)
                                light.setColor(x, y)
                                res();

                            } catch (e) {
                                console.error(e);
                                rej();
                            }

                        });
                    }
                }
            ]
        }

        let itemsMapped = menuItemsRaw.map(e => {
            return e.name
        });

        let resp = await term.singleColumnMenu(itemsMapped, {
            cancelable: true
        }).promise;

        if (!resp.canceled) {
            try {
                await menuItemsRaw[resp.selectedIndex].callback();
                await drawLight(term, light, stateEndpoint);
            } catch (e) {
                //console.log(e);
                rej();
            }
        } else {
            rej();
        }
    })
}