import fs from 'fs';
import ColorConverter from 'cie-rgb-color-converter';

export const colorpicker = function (term) {
    return new Promise(async (back, rej) => {
        let colorsFile = fs.readFileSync(process.cwd() + "/config/color.json");

        let json = JSON.parse(colorsFile);

        

        let items = json.map(e => {
            return e.name;
        })

        term.bold("\nSelect a Color\n");

        let resp = await term.singleColumnMenu(json.map(e => {
            return e.name;
        }), {
            cancelable: true
        }).promise;

        if (resp.canceled) {
            rej();
        } else {
            let selected = json[resp.selectedIndex].color;
            let color = ColorConverter.rgbToXy(selected.r, selected.g, selected.b);
            back([color.x, color.y]);
        }
    })
}